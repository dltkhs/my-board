<?php
    $record_per_page= 5;
    $page_per_block = 5;
    $now_page = ($_post['page']) ? $_post['page'] : 1;
    $now_block = ceil($now_page/$page_per_block);
    $total_page = ceil($total_record/$record_per_page);
    $total_block = ceil($total_page/$page_per_block);

    if(1<$now_block){
        $pre_page = ($now_block-1) * $page_per_block;
        echo json_encode($pre_page);
    }
    $start_page = ($now_block-1)*$page_per_block +1;
    $end_page =$now_block * $page_per_block;

    if($end_page> $total_page){
        $end_page= $total_page;
    }
    for($i=$start_page; $i<=$end_page; $i++){
        $page = $i;
        echo json_encode($page);
    }

    if($now_block< $total_block){
        $post_page = $now_block * $page_per_block + 1;
		echo json_encode($post_page);
	}
?>