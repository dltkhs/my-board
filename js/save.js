$(document).ready(function() {
    $("#save").click(function(){
        form_check();
        $("#joinForm").each(function(){
            if(!($("#join_password").val()=="")){
                this.reset();
            }
        });
    });
    $("#send").click(function(){
        write_check();
        $("#writeForm").each(function(){
            if(!($("#content").val()=="")){
                this.reset();
            }
        });
    });
    $("#login").click(function(){
        login_check();
    });
    
});
    
function login_check(){
    var login_id=$("#user_id").val();
    var login_password = $("#user_password").val();
    $.post('login.php',{
        login_id : login_id,
        login_password : login_password
    },function(){},"json").done(function(data){
        if(login_id==data.user_id&&login_password==data.user_password){
            var x = document.getElementById("login_form");
            x.removeChild(document.getElementById("login_div"));
            $("#login_success").html(login_id +"님 로그인되었습니다.");
        }
        else if(login_id==data.user_id||login_password==data.user_password){
            alert("입력이 올바르지 않습니다.");
        }
        else if(login_id!=data.user_id){
            alert("등록되어있지 않은 사용자입니다.");
        }
    });
}
    
$("#close").click(function(){
    $("#joinForm").each(function(){
                this.reset();
    });
});

$("#exit").click(function(){
    $("#writeForm").each(function(){
                this.reset();
    });
});

function form_check(){
    var join_id=$("#join_id").val();
    var join_password=$("#join_password").val();

    if(join_id==""){
        alert("아이디를 입력해주세요");
        $("#join_id").focus();
        return;
    }
    if(join_password==""){
        alert("비밀번호를 입력해주세요");
        $("#join_password").focus();
        return;
    }
    ajax_join();
}

function ajax_join(){
    $.post('save.php', {
                    name:$("#user_name").val(),
                    user_sex: $("#user_sex").val(),
                    user_birth:$("#user_birth").val(),
                    user_city:$("#user_city").val(),
                    join_id:$("#join_id").val(),
                    join_password:$("#join_password").val()
                }).done(function( data ) {
                    console.log( "Data Loaded: " + data );
                    if(data=="DBfail"){
                        //DB 접속 실패
                    }
                    else if(data=="fail"){
                        //현 정보 저장 실패
                        
                    }
                    else if(data=="success"){
                        //현 정보 저장 성공
                        $('#join').modal('hide');
                        $('#join').on('hidden.bs.modal', function(e){
                            alert("회원가입 되었습니다.");
                        });
                    }
                    else{
                    //알 수 없는 오류 발생
                    }
                
                
                });
}

function write_check(){
    var wr_title=$("#title").val();
    var wr_writer=$("#writer").val();
    var wr_content=$("#content").val();

    if(wr_title==""){
        alert("제목을 입력해주세요");
        $("#title").focus();
        return;
    }
    if(wr_writer==""){
        alert("작성자를 입력해주세요");
        $("#writer").focus();
        return;
    }
    if(wr_content==""){
        alert("내용을 입력해주세요");
        $("#content").focus();
        return;
    }
    ajax_send();
}

function ajax_send(){
    console.log("gg");
    $.post('send.php', {
        title:$("#title").val(),
        writer:$("#writer").val(),
        content:$("#content").val()
    }).done(function( data ) {                                                      
        console.log( "Data Loaded: " + data );
            if(data=="DBfail"){
                console.log("DB접속실패");
                //DB 접속 실패
            }
            else if(data=="fail"){
                console.log("정보 저장 실패");
                //현 정보 저장 실패
            }
            else if(data=="success"){
                //현 정보 저장 성공
                $('#writeboard').modal('hide');
                $('#writeboard').on('hidden.bs.modal', function(e){
                    alert("글이 저장 되었습니다.");
                });
            }
            else{
                console.log("알 수 없는 오류");
                //알 수 없는 오류 발생
            }
    });
    $("#refresh").load("board.php #refresh");
}

$(".viewdetail").click(function(){
    var detail_id = $(this).attr("data-id");
    $.post('content.php',{
        id : detail_id
    },function(){},"json").done(function(data){
        console.log(data);
        $('input[id=readtitle]').attr('value',data.title);
        $('input[id=readwriter]').attr('value',data.writer);
        $("#readcontent").val(data.content);
        $('#listdetail').modal('show');
    });
})


