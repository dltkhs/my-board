<?php
    $link = mysql_connect('localhost', 'root', 'sujin');
    mysql_select_db('userdb');

    mysql_query("SET NAMES 'utf8'");

    $record_per_page= 5;
    $page_per_block = 5;
    $now_page = ($_GET['page']) ? $_GET['page'] : 1;
    $now_block = ceil($now_page/$page_per_block);
    $first_show = $record_per_page * ($now_page - 1);
    $num_show = $record_per_page * $now_page;

    $sql = "SELECT * FROM user_write ORDER BY id desc";
    $list_result = mysql_query($sql, $link) or die("SQL error");

    $total_record = mysql_num_rows($list_result);

    $sqll = "SELECT * FROM user_write ORDER BY id desc limit $first_show,5";
    $list_resultt = mysql_query($sqll, $link) or die("SQL error");

?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title> 게시판 </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    
    <body>
        <div class="page-header">
            <div class="row">
                <div class="col-xs-5 col-md-5"><h1> &nbsp;게시판 </h1></div>
                <div class="col-xs-7 col-md-7">
                    <form class="form-inline navbar-center" method="post" id="login_form">
                        <div id="login_div">
                        ID : <input type="text" class="form-control" id="user_id" placeholder="아이디를 입력하세요">
                        Password : <input type="password" class="form-control" id="user_password" placeholder="비밀번호를 입력하세요"></div><div id="login_success"></div>
                    </form>
                    <form id="btn_form">
                        <button type="button" class="btn btn-info" id="login"> 로그인 </button>
                        <a href="#join" type="button" class="btn btn-default" data-toggle="modal"> 회원가입 </a>
                    </form>
                </div>
            </div>
        </div>
        <div id="refresh">
            <table border="1" width="80%" id="write_table" method="post">
                <tr>
                    <th width="60%" class="title"> 제목 </th>
                    <th width="20%" class="writer"> 작성자 </th>
                    <th width="20%" class="time"> 작성날짜 </th>
                </tr>
                <?php
			         for ($i = 0; $i < $total_record; $i++) {
			         // 데이터 가져오기
			         mysql_data_seek($list_result, $i);       
			         $row = mysql_fetch_array($list_resultt);
			     ?>
                <tr>
                    <td><a width="60%" class="viewdetail" href="#" data-toggle="modal" data-id="<?=$row['id']?>"><?=$row['title']?> </a></td>
                    <td width="20%" id="tablewr"><?=$row['writer']?></td>
                    <td width="20%"><?=$row['time']?></td>
                </tr>
                <?php }?>
            </table>
        </div>
            
        <div id="foot">
            <tr>
                <?php
                    $total_page = ceil($total_record/$record_per_page);
                    $total_block = ceil($total_page/$page_per_block);

                    if(1<$now_block){
                        $pre_page = ($now_block-1) * $page_per_block;
                        echo "<a href='board.php?page=".$pre_page."'> 이전 </a>";
                    }

                    $start_page = ($now_block-1)*$page_per_block +1;
                    $end_page =$now_block * $page_per_block;

                    if($end_page> $total_page){
                        $end_page= $total_page;
                    }
                ?>
                <?php
                    if($now_block< $total_block){
                        $post_page = $now_block * $page_per_block + 1;
			             echo '<a href="board.php?page='.$post_page.'">다음</a>';
                    }
                ?>
                <nav>
                    <ul class="pagination">
                        <li>
                            <a href="board.php?page=<?=$start_page?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <?php for($i=$start_page; $i<=$end_page; $i++){?>
                            <li><a href="board.php?page=<?=$i?>"><?=$i?></a></li>
                        <?php }?>
                        <li>
                            <a href="board.php?page=<?=$end_page?>" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <a href="#writeboard" type="button" id="writebtn" class="btn btn-default" data-toggle="modal"> 글쓰기 </a>
            </tr>
        </div>
        
        <div class="modal fade" id="listdetail" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>글읽기</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="readForm">
                            <p>제목 : <input type="text" id="readtitle" maxlength="30" size="31" value=""/></p>
                            <p>작성자 : <input type="text" id="readwriter" maxlength="10" value=""/>
                            </p>
                            <p>내용 <br> <textarea id="readcontent" rows="10" cols="70" value=""></textarea></p>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal" id="readexit"> 닫기</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="writeboard" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>글쓰기</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="writeForm">
                            <p>제목 : <input type="text" id="title" maxlength="30" placeholder="30자내로 입력해주세요" size="31"/></p>
                            <p>작성자 : <input type="text" id="writer" maxlength="10" placeholder="10자내로 입력해주세요"/>
                            </p>
                            <p>내용 <br> <textarea id="content" rows="10" cols="70"></textarea></p>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a id="send" class="btn btn-primary"> 올리기 </a>
                        <a class="btn btn-default" data-dismiss="modal" id="exit"> 닫기</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="join" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>회원가입</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="joinForm">
                            다음의 사용자 정보를 입력해주세요<br>
                            <p>이름 : <input type="text" id="user_name"/></p>
                            <p>성별 : <select id="user_sex">
                            <option value="남자">남자</option>
                            <option value="여자">여자</option>
                            </select>
                            </p>
                            <p>태어난 연도 : <input type="text" id="user_birth"/></p>
                            <p>사는 곳 : <select id="user_city">
                                <option value="서울" selected="selected">서울</option>
                                <option value="부산">부산</option>
                                <option value="인천">인천</option>
                                <option value="대구">대구</option>
                                <option value="광주">광주</option>
                                <option value="대전">대전</option>
                                <option value="울산">울산</option>
                                <option value="경기도">경기도</option>
                                <option value="강원도">강원도</option>
                                <option value="경상도">경상도</option>
                                <option value="전라도">전라도</option>
                                <option value="충청도">충청도</option>
                                <option value="제주도">제주도</option>
                            </select></p>          
                                <div class="form-grpup">
                                    <label for="join_id"> 아이디 &nbsp;</label><input type="button" value="중복확인" class="btn btn-success btn-xs">

                                    <input type="text" class="form-control" id="join_id" placeholder="20자내로 입력하세요">
                                </div>
                                <div class="form-grpup">
                                    <label for="join_password"> 패스워드 </label>
                                    <input type="password" class="form-control" id="join_password" placeholder="20자내로 입력하세요">
                                </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a id="save" class="btn btn-primary">가입하기</a>
                        <a class="btn btn-default" data-dismiss="modal" id="close"> 닫기</a>
                    </div>
                </div>
            </div>
        </div>
    
        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/save.js" type="text/javascript"></script>
    </body>
</html>